const listBlock = document.querySelector('.list'); 
let users = [];

const xhr = new XMLHttpRequest(); 

xhr.open('GET', 'https://rickandmortyapi.com/api/character?=results=20');
xhr.send(); 

xhr.onreadystatechange = function () {
    // console.log('state chsnge', xhr.readyState); 
    // console.log('state chsnge', xhr.status); 
    if (xhr.readyState === 4) {
        if (xhr.status === 200) {
            const response = xhr.responseText; 
            // console.log(response);
            const data = JSON.parse(response);
            // console.log(data.results);
            users = data.results; 
            console.log(users);
            renderUser(users);
        }
    }
}

function getTag (tagName, className, content=null) {
    const newTag = document.createElement(tagName);
    newTag.classList.add(className); 
    newTag.innerText = content; 
    return newTag; 
   
}

function getUserCard (user) {
    const card = getTag('div', 'user');
    const info = getTag('div', 'info'); 
    const name = getTag('span', 'name', user.name); 
    const status = getTag('span', 'status', user.status); 
    console.log (user.status)
   
    if (user.status == "Dead") {
        status.style.color = "black";     
        status.style.fontWeight = "bold";
    } if (user.status == "Alive") {
        status.style.color = "green";
        status.style.fontWeight = "bold";
    }
   
    const species = getTag('span', 'species', user.species); 
    const gender = getTag('span', 'gender', user.gender); 
    const avatar = getTag('img', 'avatar'); 
    const location = getTag('span', 'location', user.location.name)
    avatar.src = user.image;

  ; 

    info.append(name, status, species, gender, location); 
    card.append(avatar, info);

    return card; 
} 

function renderUser (users) {
    listBlock.innerHTML = ''; 
    users.forEach(item => {
        const userCard = getUserCard(item); 
        listBlock.append(userCard); 
    })
}; 

